import urllib.request
import numpy as np
import simplejson as json
import pandas as pd
#from netCDF4 import Dataset, date2num, num2date
import time
import math

class point_scraper(object):
	"""docstring for point_scraper"""
	API_key = "b741138131214c44b5b251f3c6484483"
	dataset_key_analysis = "noaa_gfs_pgrb2_global_analysis_0.25degree"
	dataset_key_forecast = "noaa_gfs_pgrb2_global_forecast_recompute_0.25degree"
	server = "http://api.planetos.com/v1/datasets/"

	def __init__(self, lon, lat, start_time, end_time):
		super(point_scraper, self).__init__()
		self.lon = lon
		self.lat = lat
		self.start_time = start_time
		self.end_time = end_time

	
	def generate_analysis_queries(self):
		point_url_analysis_tmp = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_analysis, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='tmp_m')
		point_url_analysis_wind = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_analysis, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='ugrd_m,vgrd_m')
		point_url_analysis_pres = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_analysis, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='pressfc')
		point_url_analysis_vis = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_analysis, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='Visibility_surface')

		return point_url_analysis_tmp, point_url_analysis_wind, point_url_analysis_pres, point_url_analysis_vis

	def generate_forecast_queries(self):
		point_url_forecast_tmp = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='tmp_m')
		point_url_forecast_wind = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='ugrd_m,vgrd_m')
		point_url_forecast_pres = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='pressfc')
		point_url_forecast_vis = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=5, z=0, var='Visibility_surface')

		return point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis

	def get_analysis_json(self, point_url_analysis_tmp, point_url_analysis_wind, point_url_analysis_pres, point_url_analysis_vis):
		point_data_analysis = {}
		point_data_analysis['tmp'] = json.loads(urllib.request.urlopen(point_url_analysis_tmp).read().decode('utf-8'))
		point_data_analysis['wind'] = json.loads(urllib.request.urlopen(point_url_analysis_wind).read().decode('utf-8'))
		point_data_analysis['pres'] = json.loads(urllib.request.urlopen(point_url_analysis_pres).read().decode('utf-8'))
		point_data_analysis['vis'] = json.loads(urllib.request.urlopen(point_url_analysis_vis).read().decode('utf-8'))

		for key in point_data_analysis:
			point_data_analysis[key] = point_data_analysis[key]['entries']

		return point_data_analysis

	def get_forecast_json(self, point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis):
		point_data_forecast = {}
		point_data_forecast['tmp'] = json.loads(urllib.request.urlopen(point_url_forecast_tmp).read().decode('utf-8'))
		point_data_forecast['wind'] = json.loads(urllib.request.urlopen(point_url_forecast_wind).read().decode('utf-8'))
		point_data_forecast['pres'] = json.loads(urllib.request.urlopen(point_url_forecast_pres).read().decode('utf-8'))
		point_data_forecast['vis'] = json.loads(urllib.request.urlopen(point_url_forecast_vis).read().decode('utf-8'))

		for key in point_data_forecast:
			point_data_forecast[key] = point_data_forecast[key]['entries']

		return point_data_forecast

	def get_analysis_axes_and_data(self, point_data_analysis):
		point_data_analysis_axes = {}
		point_data_analysis_data = {}

		for key in point_data_analysis:
			point_data_analysis_axes[key] = [point_data_analysis[key][i]['axes'] for i, _ in enumerate(point_data_analysis[key])]
			point_data_analysis_data[key] = [point_data_analysis[key][i]['data'] for i, _ in enumerate(point_data_analysis[key])]
		
		return point_data_analysis_axes, point_data_analysis_data

	def get_forecast_axes_and_data(self, point_data_forecast):
		point_data_forecast_axes = {}
		point_data_forecast_data = {}

		for key in point_data_forecast:
			point_data_forecast_axes[key] = [point_data_forecast[key][i]['axes'] for i, _ in enumerate(point_data_forecast[key])]
			point_data_forecast_data[key] = [point_data_forecast[key][i]['data'] for i, _ in enumerate(point_data_forecast[key])]
		
		return point_data_forecast_axes, point_data_forecast_data

	def subset_forecast_axes_and_data(self, reftime, point_data_forecast_axes, point_data_forecast_data):
		reftime_check = [point_data_forecast_axes['tmp'][i]['reftime'] == reftime for i, _ in enumerate(point_data_forecast_axes['tmp'])]
		# indices where forecast reftime equals current analysis time
		reftime_indices = [i for i, x in enumerate(reftime_check) if x]
		#subset forecast data
		for key in point_data_forecast_axes:
			point_data_forecast_axes[key] = point_data_forecast_axes[key][reftime_indices[0]:reftime_indices[-1] + 1]
			point_data_forecast_data[key] = point_data_forecast_data[key][reftime_indices[0]:reftime_indices[-1] + 1]

		return point_data_forecast_axes, point_data_forecast_data

	def get_analysis_pandas_df(self, point_data_analysis_axes, point_data_analysis_data):
		pd_temp_analysis_tmp = pd.DataFrame(point_data_analysis_axes['tmp'])
		pd_temp_analysis_wind = pd.DataFrame(point_data_analysis_axes['wind'])
		pd_temp_analysis_pres = pd.DataFrame(point_data_analysis_axes['pres'])
		pd_temp_analysis_vis = pd.DataFrame(point_data_analysis_axes['vis'])
		pd_temp_analysis_tmp['reftime'] = pd_temp_analysis_tmp['time'][0]
		pd_temp_analysis_wind['reftime'] = pd_temp_analysis_wind['time'][0]
		pd_temp_analysis_pres['reftime'] = pd_temp_analysis_pres['time'][0]
		pd_temp_analysis_vis['reftime'] = pd_temp_analysis_vis['time'][0]
		pd_temp_analysis_tmp['dataset'] = 'analysis'
		pd_temp_analysis_wind['dataset'] = 'analysis'
		pd_temp_analysis_pres['dataset'] = 'analysis'
		pd_temp_analysis_vis['dataset'] = 'analysis'
		pd_temp_analysis_tmp = pd.concat([pd_temp_analysis_tmp, pd.DataFrame(point_data_analysis_data['tmp'])] , axis=1)
		pd_temp_analysis_wind = pd.concat([pd_temp_analysis_wind, pd.DataFrame(point_data_analysis_data['wind'])] , axis=1)
		pd_temp_analysis_pres = pd.concat([pd_temp_analysis_pres, pd.DataFrame(point_data_analysis_data['pres'])] , axis=1)
		pd_temp_analysis_vis = pd.concat([pd_temp_analysis_vis, pd.DataFrame(point_data_analysis_data['vis'])] , axis=1)

		return pd_temp_analysis_tmp, pd_temp_analysis_wind, pd_temp_analysis_pres, pd_temp_analysis_vis

	def get_forecast_pandas_df(self, point_data_forecast_axes, point_data_forecast_data):
		pd_temp_forecast_tmp = pd.DataFrame(point_data_forecast_axes['tmp'])
		pd_temp_forecast_wind = pd.DataFrame(point_data_forecast_axes['wind'])
		pd_temp_forecast_pres = pd.DataFrame(point_data_forecast_axes['pres'])
		pd_temp_forecast_vis = pd.DataFrame(point_data_forecast_axes['vis'])
		pd_temp_forecast_tmp['dataset'] = 'forecast'
		pd_temp_forecast_wind['dataset'] = 'forecast'
		pd_temp_forecast_pres['dataset'] = 'forecast'
		pd_temp_forecast_vis['dataset'] = 'forecast'
		pd_temp_forecast_tmp = pd.concat([pd_temp_forecast_tmp, pd.DataFrame(point_data_forecast_data['tmp'])] , axis=1)
		pd_temp_forecast_wind = pd.concat([pd_temp_forecast_wind, pd.DataFrame(point_data_forecast_data['wind'])] , axis=1)
		pd_temp_forecast_pres = pd.concat([pd_temp_forecast_pres, pd.DataFrame(point_data_forecast_data['pres'])] , axis=1)
		pd_temp_forecast_vis = pd.concat([pd_temp_forecast_vis, pd.DataFrame(point_data_forecast_data['vis'])] , axis=1)

		return pd_temp_forecast_tmp, pd_temp_forecast_wind, pd_temp_forecast_pres, pd_temp_forecast_vis

	def get_combined_dataframes(self, pd_temp_analysis_df, pd_temp_forecast_df):
		final_dataframe = pd.concat([pd_temp_analysis_df, pd_temp_forecast_df], ignore_index=True)

		return final_dataframe

	def convert_kelvin_to_C(self, final_dataframe_tmp):
		final_dataframe_tmp['tmp_m'] -= 273.15

		return final_dataframe_tmp

	def convert_uv_to_windspeed(self, final_dataframe_wind):
		final_dataframe_wind['windgrd_m'] = np.sqrt(final_dataframe_wind['ugrd_m']**2 + final_dataframe_wind['vgrd_m']**2)

		return final_dataframe_wind

	def convert_uv_to_winddir(self, final_dataframe_wind):
		direction = 180 + np.arctan2(final_dataframe_wind['ugrd_m'], final_dataframe_wind['vgrd_m'])*180/math.pi
		for i, _ in enumerate(direction):
			if direction[i] == 360.0:
				direction[i] = 0 

		final_dataframe_wind['winddir'] = direction

		return final_dataframe_wind

	def drop_uv_from_dataframe(self, final_dataframe_wind):
		final_dataframe_wind.drop('ugrd_m', axis=1, inplace=True)
		final_dataframe_wind.drop('vgrd_m', axis=1, inplace=True)

		return final_dataframe_wind

	def convert_pressure_Pa_hPa(self, final_dataframe_pres):
		final_dataframe_pres['pressfc'] *= 0.01

		return final_dataframe_pres

	def convert_vis_m_to_km(self, final_dataframe_vis):
		final_dataframe_vis['Visibility_surface'] *= 0.001

		return final_dataframe_vis

	def round_column_to_precision(self, final_dataframe, column_name, precision=1):
		final_dataframe = final_dataframe.round({column_name: precision})

		return final_dataframe


class point_scraper_forecast(point_scraper):
	"""only scrape the forecast"""
	# superclass constructor:
	def __init__(self, lon, lat, start_time, end_time):
		super(point_scraper_forecast, self).__init__(lon, lat, start_time, end_time)

	def generate_forecast_queries(self, count):
		point_url_forecast_tmp = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=count, z=0, var='tmp_m')
		point_url_forecast_wind = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=count, z=0, var='ugrd_m,vgrd_m')
		point_url_forecast_pres = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=count, z=0, var='pressfc')
		point_url_forecast_vis = generate_point_api_query(server=self.server, dataset_key=self.dataset_key_forecast, longitude=self.lon, latitude=self.lat, API_key=self.API_key, start=self.start_time, end=self.end_time, count=count, z=0, var='Visibility_surface')

		return point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis







def generate_point_api_query(server, dataset_key, longitude, latitude, API_key, count=20, z='all', **kwargs):
	returl = server + dataset_key + "/point?lat={0}&lon={1}&apikey={2}&count={3}&z={4}".format(latitude,longitude,API_key,count,z)
	for i,j in kwargs.items():
		returl += "&{0}={1}".format(i,j)

	return returl

def write_data_to_csv(fname, final_dataframe):
	with open(fname, 'w') as f:
		final_dataframe.to_csv(f, index=False)

	return


def write_new_data_to_csv(fname, final_dataframe):
	with open(fname, 'a') as f:
		final_dataframe.to_csv(f, header=False, index=False)

	return