import argparse
import GFS_scraper

parser = argparse.ArgumentParser(description='CLI for GFS forecast.')

parser.add_argument('lon', metavar='lon', type=float)
parser.add_argument('lat', metavar='lat', type=float)
parser.add_argument('start_time', metavar='start_time', type=str)
parser.add_argument('end_time', metavar='end_time', type=str)

args = parser.parse_args()
print("\n")
print("Lon: " + str(args.lon))
print("Lat: " + str(args.lat))
print("Start: " + args.start_time)
print("End: " + args.end_time)
print("\n")

print("Generating scraper instance..")
scraper = GFS_scraper.point_scraper_forecast(args.lon, args.lat, args.start_time, args.end_time)

print("Querying JSON..")
point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis = scraper.generate_forecast_queries(count=20)
point_data_forecast = scraper.get_forecast_json(point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis)
point_data_forecast_axes, point_data_forecast_data = scraper.get_forecast_axes_and_data(point_data_forecast)

print("Generating dataframes..")
pd_temp_forecast_tmp, pd_temp_forecast_wind, pd_temp_forecast_pres, pd_temp_forecast_vis = scraper.get_forecast_pandas_df(point_data_forecast_axes, point_data_forecast_data)

final_dataframe_tmp = scraper.convert_kelvin_to_C(pd_temp_forecast_tmp)
final_dataframe_wind = scraper.convert_uv_to_windspeed(pd_temp_forecast_wind)
final_dataframe_wind = scraper.convert_uv_to_winddir(pd_temp_forecast_wind)
final_dataframe_wind = scraper.drop_uv_from_dataframe(pd_temp_forecast_wind)
final_dataframe_pres = scraper.convert_pressure_Pa_hPa(pd_temp_forecast_pres)
final_dataframe_vis = scraper.convert_vis_m_to_km(pd_temp_forecast_vis)

final_dataframe_tmp = scraper.round_column_to_precision(final_dataframe_tmp, 'tmp_m', 1)
final_dataframe_wind = scraper.round_column_to_precision(final_dataframe_wind, 'windgrd_m', 1)
final_dataframe_wind = scraper.round_column_to_precision(final_dataframe_wind, 'winddir', 0)
final_dataframe_pres = scraper.round_column_to_precision(final_dataframe_pres, 'pressfc', 1)
final_dataframe_vis = scraper.round_column_to_precision(final_dataframe_vis, 'Visibility_surface', 1)

fname_gen = lambda param: 'gfs_' + str(param) + "_" + str(args.lon).replace(".","") + "_" + str(args.lat).replace(".","") + '.csv'

print("Writing data to csv, which is in fact one of the best data storage media..")
GFS_scraper.write_data_to_csv(fname_gen('tmp'), final_dataframe_tmp)
GFS_scraper.write_data_to_csv(fname_gen('wind'), final_dataframe_wind)
GFS_scraper.write_data_to_csv(fname_gen('pres'), final_dataframe_pres)
GFS_scraper.write_data_to_csv(fname_gen('vis'), final_dataframe_vis)