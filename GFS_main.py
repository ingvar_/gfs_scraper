import GFS_scraper
from datetime import datetime, timedelta

def generate_start_and_end_time(startyear, startmonth, startday, starthour):
	if startmonth < 10:
		startmonth = "0" + str(startmonth)

	if startday < 10:
		startday = "0" + str(startday)

	if (starthour >= 0 and starthour < 6):
		starthour = 0
	elif (starthour >= 6 and starthour < 12):
		starthour = 6
	elif (starthour >= 12 and starthour < 18):
		starthour = 12
	else:
		starthour = 18

	if starthour < 10:
		starthour = "0" + str(starthour)

	endhour = int(starthour) + 5

	if endhour < 10:
		endhour = "0" + str(endhour)

	start = "{}-{}-{}T{}:00:00".format(startyear, startmonth, startday, starthour)
	end = "{}-{}-{}T{}:00:00".format(startyear, startmonth, startday, endhour)

	return start, end

# ------
# the full scraping script starts here:
# ------

# subtract one day from current date
working_dir = "/home/ubuntu/gfs_scraper/"

d = datetime.today() - timedelta(days=1)

curr_year = d.year
curr_month = d.month
curr_day = d.day
curr_hour = d.hour

#for day in [2]:
#	for hour in [6]:
#		start, end = generate_start_and_end_time(2017, 8, day, hour)

start, end = generate_start_and_end_time(curr_year, curr_month, curr_day, curr_hour)
	
print("Current start and end timestamps: {}, {}".format(start, end))
print("Generating scraper object..")

scraper = GFS_scraper.point_scraper(24.5, 59.5, start, end)

print("Querying JSON..")
point_url_analysis_tmp, point_url_analysis_wind, point_url_analysis_pres, point_url_analysis_vis = scraper.generate_analysis_queries()
point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis = scraper.generate_forecast_queries()

for attempt in range(5):
	print("Attempt {}".format(attempt))
	try:
		point_data_analysis = scraper.get_analysis_json(point_url_analysis_tmp, point_url_analysis_wind, point_url_analysis_pres, point_url_analysis_vis)
		point_data_forecast = scraper.get_forecast_json(point_url_forecast_tmp, point_url_forecast_wind, point_url_forecast_pres, point_url_forecast_vis)
		print("Success.")
	except urllib.error.HTTPError:
		continue

point_data_analysis_axes, point_data_analysis_data = scraper.get_analysis_axes_and_data(point_data_analysis)
point_data_forecast_axes, point_data_forecast_data = scraper.get_forecast_axes_and_data(point_data_forecast)

print("Subsetting forecast data..")
point_data_forecast_axes, point_data_forecast_data = scraper.subset_forecast_axes_and_data(start, point_data_forecast_axes, point_data_forecast_data)

print("Generating dataframes..")
pd_temp_analysis_tmp, pd_temp_analysis_wind, pd_temp_analysis_pres, pd_temp_analysis_vis = scraper.get_analysis_pandas_df(point_data_analysis_axes, point_data_analysis_data)
pd_temp_forecast_tmp, pd_temp_forecast_wind, pd_temp_forecast_pres, pd_temp_forecast_vis = scraper.get_forecast_pandas_df(point_data_forecast_axes, point_data_forecast_data)

final_dataframe_tmp = scraper.get_combined_dataframes(pd_temp_analysis_tmp, pd_temp_forecast_tmp)
final_dataframe_wind = scraper.get_combined_dataframes(pd_temp_analysis_wind, pd_temp_forecast_wind)
final_dataframe_pres = scraper.get_combined_dataframes(pd_temp_analysis_pres, pd_temp_forecast_pres)
final_dataframe_vis = scraper.get_combined_dataframes(pd_temp_analysis_vis, pd_temp_forecast_vis)

final_dataframe_tmp = scraper.convert_kelvin_to_C(final_dataframe_tmp)
final_dataframe_wind = scraper.convert_uv_to_windspeed(final_dataframe_wind)
final_dataframe_wind = scraper.convert_uv_to_winddir(final_dataframe_wind)
final_dataframe_wind = scraper.drop_uv_from_dataframe(final_dataframe_wind)
final_dataframe_pres = scraper.convert_pressure_Pa_hPa(final_dataframe_pres)
final_dataframe_vis = scraper.convert_vis_m_to_km(final_dataframe_vis)

final_dataframe_tmp = scraper.round_column_to_precision(final_dataframe_tmp, 'tmp_m', 1)
final_dataframe_wind = scraper.round_column_to_precision(final_dataframe_wind, 'windgrd_m', 1)
final_dataframe_wind = scraper.round_column_to_precision(final_dataframe_wind, 'winddir', 0)
final_dataframe_pres = scraper.round_column_to_precision(final_dataframe_pres, 'pressfc', 1)
final_dataframe_vis = scraper.round_column_to_precision(final_dataframe_vis, 'Visibility_surface', 1)

print("Writing new data to csv, which is in fact one of the best data storage media..")
GFS_scraper.write_new_data_to_csv(working_dir + 'gfs_tmp_595_245.csv', final_dataframe_tmp)
GFS_scraper.write_new_data_to_csv(working_dir + 'gfs_wind_595_245.csv', final_dataframe_wind)
GFS_scraper.write_new_data_to_csv(working_dir + 'gfs_pres_595_245.csv', final_dataframe_pres)
GFS_scraper.write_new_data_to_csv(working_dir + 'gfs_vis_595_245.csv', final_dataframe_vis)
